# RELEASE NOTES

## Release 004 (13/3/2018)

### Added
1. **Formateador de dominios**: filter

### Fixed
1. Formato de Dominio en distintas vistas


## Release 003 (12/3/2018)

### Fixed

1. **Turno vista Dominio User**: corrección de typos, información adicional del dominio, acción de cancelar
2. **Turno vista Servicio User**: acción de cancelar
3. **Turno vista Calendario User**: corrección typos
4. **Turno vista Horario User**: corrección de typos, acción de cancelar
5. **Turno vista Confirmar User**: corrección de typos, formato de dato, representación de datos, Terminos y condiciones y funcionamiento condicional del botón Confirmar
6. **Turnos Controller**: Ajuste en los modals y popup, corrección de typos, formato de datos y representación de datos.
7. **Vehículos vista**: corrección de typos, formato de datos y representación de datos. Se incluyo datos faltantes.
8. **Vehículos Controller**: Ajuste de modals y popups, corrección de typos formato de datos y representación de datos.

### Added
1. **Panel Controller**: Selector de rango para próximos turnos y corte por día
2. **Turnos Controller**: se incorporó función de reagendar turnos


## Release 002 (12/3/2018)

### Fixed

1. Carga del Calendario
2. Vista Solicitar turno
3. Comportamiento del Registro


## Release 001 (2/3/2018)

...







-----------------------------------------------------------------------------------------------
Each version should:
Date List its release date in the format YYYY-MM-DD.
##  Semantic Versioning code: Group changes to describe their impact on the project, as follows:
### Added for new features.
### Changed for changes in existing functionality.
### Deprecated for once-stable features removed in upcoming releases.
### Removed for deprecated features removed in this release.
### Fixed for any bug fixes.
### Security to invite users to upgrade in case